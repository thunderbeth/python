import urllib.request
import re

def dumpdwnld (langcode):
    homepage = urllib.request.urlopen('http://dumps.wikimedia.org/backup-index.html')
    homepage_text = homepage.read().decode('utf-8')
    result = 'http://dumps.wikimedia.org/' + langcode + 'wiki/[0-9]+'
    result1 = re.search(result, homepage_text)
    return result1
    file = langcode + 'wiki-[0-9]+-pages-articles.xml.bz2'
    urllib.request.urlretrieve(result1, file)

langcode = input('Type your language code: ')
dumpdwnld (langcode)



