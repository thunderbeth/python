import re, codecs, time

start = time.clock()

bad_poems = codecs.open('long_poem.txt', 'r', encoding = 'utf-8')
words = codecs.open('words.txt', 'r', encoding = 'utf-8')
snippets = codecs.open('snippets.txt', 'w', encoding = 'utf-8')

lines = []
words1 = []
snippet = []

for line in bad_poems:
    lines.append(line)
for line in words:
    words1.append(line)


for i in range(len(lines)):
    for j in range(len(words1)):
        if words1[j] in lines[i]:
            if i == len(lines)-1:
                snippet.append(lines[i-1])
                snippet.append(lines[i])
            elif words1[j] in lines[0]:
                snippet.append(lines[i])
                snippet.append(lines[i + 1])
            else:
                snippet.append(lines[i-1])
                snippet.append(lines[i])
                snippet.append(lines[i+1])
            break

for a in range(len(snippet)):
    snippets.write(snippet[a])


bad_poems.close()
words.close()
snippets.close()



print (time.clock() - start)


